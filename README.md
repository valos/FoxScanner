FoxScanner
=====
Firefox OS Mobile Scanner Webapp

FoxScanner turns your phone into a mobile scanner.
It helps you scan any type of document, transform it into PDF format and share it right from your device.

Licensing
===
 * FoxScanner → GNU Affero GPL v3.0
 * Building-Blocks → Apache License v2.0
 * jQuery → MIT
 * Jcrop → MIT
 * glfx.js → MIT
 * pica → MIT
 * jsPDF → MIT
 * spin.js → MIT
