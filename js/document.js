/**
 * FoxScanner
 *
 * written by Valéry Febvre
 * vfebvre@aester-eggs.com
 *
 * Copyright 2015 Valéry Febvre
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function FoxScannerDocument() {
    var $section = $('#document');

    var $editDialog = $('#document-edit-dialog', $section);
    var $pageActionDialog = $('#document-page-action-dialog', $section);
    var $removeDialog = $('#document-remove-dialog', $section);
    var $pageRemoveDialog = $('#document-page-remove-dialog', $section);
    var $pageSaveDialog = $('#document-page-save-dialog', $section);

    // current loaded document
    var current;

    $('#document-close', $section).on('click', function(e) {
        close();
    });

    /*
     pages action dialog
     move up/down, remove, share, save
     */
    $(document).on('contextmenu', function(e) {
        e.preventDefault();
        if (e.target.fileIndex === undefined) {
            return;
        }
        current.fileIndex = e.target.fileIndex;
        $('header', $pageActionDialog).text('Page ' + (e.target.fileIndex + 1));
        $('#document-page-action-move-up', $pageActionDialog).prop('disabled', current.fileIndex === 0);
        $('#document-page-action-move-down', $pageActionDialog).prop('disabled', current.fileIndex === current.files.length - 1);
        $('#document-page-action-remove', $pageActionDialog).prop('disabled', current.record.filenames.length === 1);
        $pageActionDialog.removeClass('hide').addClass('show');
    });
    $('#document-page-action-cancel', $pageActionDialog).on('click', function(e) {
        $pageActionDialog.removeClass('show').addClass('hide');
    });
    // move up
    $('#document-page-action-move-up', $pageActionDialog).on('click', function(e) {
        $pageActionDialog.removeClass('show').addClass('hide');
        movePage(-1);
    });
    // move down
    $('#document-page-action-move-down', $pageActionDialog).on('click', function(e) {
        $pageActionDialog.removeClass('show').addClass('hide');
        movePage(1);
    });
    // remove
    $('#document-page-action-remove', $pageActionDialog).on('click', function(e) {
        $pageActionDialog.removeClass('show').addClass('hide');
        $pageRemoveDialog.removeClass('hide').addClass('show');
    });
    $('#document-page-remove-cancel', $pageRemoveDialog).on('click', function(e) {
        $pageRemoveDialog.removeClass('show').addClass('hide');
    });
    $('#document-page-remove-validate', $pageRemoveDialog).on('click', function(e) {
        removePage();
        $pageRemoveDialog.removeClass('show').addClass('hide');
    });
    // share
    $('#document-page-action-share', $pageActionDialog).on('click', function(e) {
        $pageActionDialog.removeClass('show').addClass('hide');
        sharePage();
    });
    // save
    $('#document-page-action-save', $pageActionDialog).on('click', function(e) {
        $pageActionDialog.removeClass('show').addClass('hide');
        $('#document-page-save-filename', $pageSaveDialog).val('');
        $pageSaveDialog.removeClass('hide').addClass('show');
    });
    $('#document-page-save-cancel', $pageSaveDialog).on('click', function(e) {
        $pageSaveDialog.removeClass('show').addClass('hide');
    });
    $('#document-page-save-validate', $pageSaveDialog).on('click', function(e) {
        var filename = $('#document-page-save-filename', $pageSaveDialog).val().trim();
        savePage(filename);
    });

    // edit dialog
    $('#document-edit', $section).on('click', function(e) {
        $editDialog.removeClass('hide').addClass('show');
        $('#document-title', $editDialog).val(current.record.title);
        $('#document-description', $editDialog).val(current.record.description);
    });
    $('#document-edit-cancel', $editDialog).on('click', function(e) {
        $editDialog.removeClass('show').addClass('hide');
    });
    $('#document-edit-validate', $editDialog).on('click', function(e) {
        e.preventDefault();
        edit();
        $editDialog.removeClass('show').addClass('hide');
    });

    // remove dialog
    $('#document-remove', $section).on('click', function(e) {
        $removeDialog.removeClass('hide').addClass('show');
    });
    $('#document-remove-cancel', $removeDialog).on('click', function(e) {
        $removeDialog.removeClass('show').addClass('hide');
    });
    $('#document-remove-validate', $removeDialog).on('click', function(e) {
        remove();
        $removeDialog.removeClass('show').addClass('hide');
    });

    $('#document-add-picture', $section).on('click', App.documents.takePicture);

    $('#document-share', $section).on('click', share);

    function createOrAddPicture(dataURL, width, height) {
        var create = !current;
        // used a fake image extension _jpg so that images don't appear in Gallery app
        var filename = new Date().getTime() + '._jpg';
        var title = navigator.mozL10n.get('new-document');
        var transaction = App.db.transaction(['documents'], 'readwrite');
        var store = transaction.objectStore('documents');
        var request;

        if (create) {
            request = store.add({
                title: title,
                description: '',
                filenames: [filename],
                created: new Date(),
                updated: new Date()
            });
        }
        else {
            current.record.updated = new Date();
            current.record.filenames.push(filename);
            request = store.put(current.record);
        }

        request.onsuccess = function(e) {
            var id, request;
            var dataEncoded = dataURL.substring(dataURL.indexOf(',') + 1, dataURL.length);
            var data = Base64Binary.decode(dataEncoded);
            var blob = new Blob(
                [data],
                {
                    type: 'image/jpeg'
                }
            );

            if (create) {
                id = e.target.result;
            }
            else {
                id = current.record.id;
            }
            request = App.sdcard.addNamed(blob, getFilepath(id, filename));
            request.onsuccess = function() {
                load(id);

                // clear cropper & enhancer
                App.cropper.clear();
                App.enhancer.clear();

                if (create) {
                    createThumbnail(id, dataURL, 100, height / (width / 100));
                }
                else {
                    App.documents.render();
                }
            };
            request.onerror = function(e) {
                // rollback
                transaction.abort();
                App.activityIndicator.stop();
                utils.status.show('Unable to write the file: ' + e.target.error.name);
            };
        };
        request.onerror = function(e) {
            App.activityIndicator.stop();
            utils.status.show("Error in saving the document in DB. Reason: " + e.value);
        };
    }

    function createThumbnail(id, dataURL, width, height) {
        var img = new Image();

        img.onload = function() {
            var canvas;
            var tnDataURL, tnDataEncoded, tnData;
            var blob;
            var request;

            canvas = document.createElement("canvas");
            canvas.width = width;
            canvas.height = height;
            canvas.getContext("2d").drawImage(img, 0, 0, width, height);

            tnDataURL = canvas.toDataURL('image/jpeg', 0.85);
            tnDataEncoded = tnDataURL.substring(tnDataURL.indexOf(',') + 1, tnDataURL.length);
            tnData = Base64Binary.decode(tnDataEncoded);
            blob = new Blob(
                [tnData],
                {
                    type: 'image/jpeg'
                }
            );

            // used a fake image extension _jpg so that thumbnails don't appear in Gallery app
            request = App.sdcard.addNamed(blob, getFilepath(id, 'thumbnail._jpg'));
            request.onsuccess = function() {
                App.documents.render();                
            };
            request.onerror = function(e) {
                utils.status.show('Unable to write the thumbnail file');
            };
        };

        img.src = dataURL;
    }

    function edit() {
        var store;
        var request;
        var title = $('#document-title').val().trim();
        var description = $('#document-description').val();

        if (current.record.title === title && current.record.description === description) {
            return;
        }
        if (title === '') {
            title = current.record.title;
        }

        store = App.db.transaction(["documents"], "readwrite").objectStore("documents");
        current.record.title = title;
        current.record.description = description;
        request = store.put(current.record);
        request.onsuccess = function() {
            App.documents.render();
        };
        request.onerror = function(e) {
            utils.status.show("Error in editing the document");
        };
    }

    function getFolderpath(id) {
        return App.name + '/' + id;
    }

    function getFilepath(id, filename) {
        return getFolderpath(id) + '/' + filename;
    }

    function load(id) {
        var request;

        App.activityIndicator.start();

        request = App.db.transaction(["documents"]).objectStore("documents").get(id);
        request.onsuccess = function() {
            var record = request.result;

            if (record.description === undefined) {
                // Description was introduced in v1.1.0
                record.description = '';
            }
            current = {
                record: record,
                files: []
            };

            $('.picture-container', $section).empty();
            $.each(current.record.filenames, function(i, filename) {
                var request = App.sdcard.get(getFilepath(id, filename));
                request.onsuccess = function() {
                    var file = this.result;
                    var image = new Image();

                    image.width = $('.picture-container', $section).width();
                    image.fileIndex = i;
                    image.onload = function(e) {
                        // saved File object and size of each file
                        current.files[e.target.fileIndex] = {
                            obj: file,
                            size: [image.naturalWidth, image.naturalHeight]
                        };
                        if (i === current.record.filenames.length - 1) {
                            // last image of document has been loaded
                            App.activityIndicator.stop();
                            open();
                        }
                        window.URL.revokeObjectURL(image.src);
                    };
                    image.src = window.URL.createObjectURL(file);

                    $('.picture-container', $section).append(image);
                    if (i < current.record.filenames.length - 1) {
                        $('.picture-container', $section).append('<hr>');
                    }
                };

                request.onerror = function(e) {
                    utils.status.show("Unable to get the file " + getFilepath(id, filename));
                    if (i === current.record.filenames.length - 1) {
                        App.activityIndicator.stop();
                        open();
                    }
                };
            });
        };
        request.onerror = function(e) {
            App.activityIndicator.stop();
        };
    }

    function movePage(direction) {
        // up if direction == -1
        // down if direction == 1
        var request;
        var store = App.db.transaction(["documents"], "readwrite").objectStore("documents");

        var tmp = current.record.filenames[current.fileIndex + direction];
        current.record.filenames[current.fileIndex + direction] = current.record.filenames[current.fileIndex];
        current.record.filenames[current.fileIndex] = tmp;

        request = store.put(current.record);
        request.onsuccess = function() {
            load(current.record.id);
            $pageActionDialog.removeClass('show').addClass('hide');
        };
        request.onerror = function(e) {
            if (direction === -1) {
                utils.status.show("Unable to move the page up");
            }
            else if (direction === 1) {
                utils.status.show("Unable to move the page down");
            }
            $pageActionDialog.removeClass('show').addClass('hide');
        };
    }

    function open() {
        App.$currentSection.attr('class', 'currentToLeft');
        $section.attr('class', 'rightToCurrent');
        App.$currentSection = $section;
    }

    function close() {
        current = null;
        App.$currentSection = $('#documents');
        $section.attr('class', 'currentToRight');
        $('#documents').attr('class', 'leftToCurrent');
    }

    function processFile(index, processCb) {
        var reader = new FileReader();

        reader.onloadend = function(e) {
            var dataURL = e.target.result;

            // fixed mimetype in dataURL
            // with an invalid image extension (._jpg), images are read as application/octet-stream
            dataURL = dataURL.replace('application/octet-stream', 'image/jpeg');
            var dataEncoded = dataURL.substring(dataURL.indexOf(',') + 1, dataURL.length);
            var data = Base64Binary.decode(dataEncoded);
            var blob = new Blob(
                [data],
                {
                    type: 'image/jpeg'
                }
            );

            processCb(blob);
        };

        reader.readAsDataURL(current.files[index].obj);
    }

    function remove() {
        var request = App.db.transaction(["documents"], "readwrite").objectStore("documents").delete(current.record.id);
        request.onsuccess = function(e) {
            var request = App.sdcard.delete(getFolderpath(current.record.id));
            request.onerror = function () {
                utils.status.show("Unable to delete the document");
            };

            App.documents.render();
            utils.status.show(navigator.mozL10n.get('document-deleted'));
            close();
        };
    }

    function removePage() {
        var filename = current.record.filenames[current.fileIndex];
        var store = App.db.transaction(["documents"], "readwrite").objectStore("documents");
        var request;

        current.record.filenames.splice(current.fileIndex, 1);
        request = store.put(current.record);
        request.onsuccess = function() {
            var request = App.sdcard.delete(getFilepath(current.record.id, filename));
            request.onsuccess = function() {
                load(current.record.id);
                App.documents.render();
                $pageActionDialog.removeClass('show').addClass('hide');
            };
            request.onerror = function () {
                utils.status.show("Unable to delete the page");
            };
        };
        request.onerror = function(e) {
            utils.status.show("Unable to delete the page");
            $pageActionDialog.removeClass('show').addClass('hide');
        };
    }

    function share() {
        var counter = 0;
        var dataURLs = [];

        function buildPDF() {
            var pdf = new jsPDF('p', 'mm', 'a4');
            pdf.setProperties({
                title: current.record.title,
                creator: App.name
            });

            $.each(dataURLs, function(i, dataURL) {
                var size = current.files[i].size;
                var margin, x, y, width, height;

                margin = 0;
                x = margin;
                y = margin;
                width = 210 - margin * 2;
                height = width * (size[1] / size[0]);
                if (height > 297 - margin * 2) {
                    height = 297 - margin * 2;
                    width = height * (size[0] / size[1]);
                    x += (210 - margin * 2 - width) / 2;
                }

                pdf.addImage(dataURL, 'JPEG', x, y, width, height);

                if (i === dataURLs.length - 1) {
                    new MozActivity({
                        name: 'new',
                        data: {
                            type : 'mail',
                            blobs: [pdf.output('blob')],
                            filenames: [current.record.title + '.pdf'],
                            url: 'mailto:?subject=' + current.record.title + '&body=' + current.record.description
                        }
                    });
                }
                else {
                    pdf.addPage();
                } 
            });
        }

        $.each(current.files, function(i, file) {
            var reader = new FileReader();

            reader.fileIndex = i;
            reader.onloadend = function(e) {
                var dataURL = e.target.result;
                counter += 1;

                // fixed mimetype in dataURL
                // with an invalid image extension (._jpg), images are read as application/octet-stream
                dataURL = dataURL.replace('application/octet-stream', 'image/jpeg');
                dataURLs[e.target.fileIndex] = dataURL;

                if (counter === current.files.length) {
                    buildPDF();
                }
            };

            reader.readAsDataURL(file.obj);
        });
    }

    function savePage(filename) {
        if (!filename) {
            return;
        }

        processFile(current.fileIndex, function(blob) {
            var request = App.picturesStorage.addNamed(blob, filename + '.jpg');

            request.onsuccess = function () {
                var name = this.result;
                utils.status.show(navigator.mozL10n.get('page-saved-success'));
                $pageSaveDialog.removeClass('show').addClass('hide');
            };

            // An error typically occur if a file with the same name already exist
            request.onerror = function () {
                utils.status.show(navigator.mozL10n.get('page-saved-error'), 3000);
            };
        });
    }

    function sharePage() {
        processFile(current.fileIndex, function(blob) {
            var filename = current.record.title + '-page-' + (current.fileIndex + 1) + '.jpg';

            new MozActivity({
                name: 'share',
                data: {
                    type: 'image/*',
                    number: 1,
                    blobs: [blob],
                    filenames: [filename]
                }
            });
        });
    }

    return {
        createOrAddPicture: createOrAddPicture,
        getFilepath: getFilepath,
        load: load
    };
}
