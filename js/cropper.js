/**
 * FoxScanner
 *
 * written by Valéry Febvre
 * vfebvre@aester-eggs.com
 *
 * Copyright 2015 Valéry Febvre
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function FoxScannerCropper() {
    var $section = $('#cropper');
    var $picture = null;
    var jcrop = null;
    var pictureInfo = {};
    var state = {};

    $('#cropper-close').on('click', function() {
        close();
    });
    $('#cropper-validate').on('click', function() {
        App.enhancer.load();
    });

    function clear() {
        if (pictureInfo.url) {
            URL.revokeObjectURL(pictureInfo.url);
        }
        if (jcrop) {
            jcrop.destroy();
            $picture.remove();
        }
    }

    function load(url) {
        var URL = window.URL || window.webkitURL;
        var canvas = document.createElement("canvas");
        var ctx = canvas.getContext('2d');
        var image = new Image();
        var width = $('.picture-container', $section).width();

        pictureInfo.url = url;

        image.onload = function () {
            pictureInfo.width = image.naturalWidth;
            pictureInfo.height = image.naturalHeight;

            if (pictureInfo.width <= pictureInfo.height) {
                state.rotation = 0;
                pictureInfo.ratio = pictureInfo.width / width;

                canvas.width = width;
                canvas.height = pictureInfo.height / pictureInfo.ratio;

                ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
            }
            else {
                //state.rotation = 90;
                state.rotation = 270;
                pictureInfo.ratio = pictureInfo.height / width;

                canvas.width = width;
                canvas.height = pictureInfo.width / pictureInfo.ratio;

                //ctx.translate(canvas.width, 0); // rotation 90
                ctx.translate(0, canvas.height); // rotation 270
                ctx.rotate(state.rotation * Math.PI / 180);

                ctx.drawImage(image, 0, 0, canvas.height, canvas.width);
            }

            canvas.id = 'picture';
            $('.picture-container', $section).append(canvas);
            $picture = $('#picture');

            $picture.Jcrop(
                {
                    allowSelect: false,
                    keySupport: false,
                    minSize: [40, 40],
                    setSelect: [20, 20, canvas.width - 20, canvas.height - 20]
                },
                function() {
                    jcrop = this;
                    open();
                }
            );
        };
        image.src = pictureInfo.url;
    }

    function open(from) {
        if (from === 'enhancer') {
            App.$currentSection.attr('class', 'currentToRight');
            $section.attr('class', 'leftToCurrent');
        }
        else {
            App.$currentSection.attr('class', 'currentToLeft');
            $section.attr('class', 'rightToCurrent');
        }
        App.$currentSection = $section;
    }

    function close() {
        App.$currentSection = $('#documents');
        $section.attr('class', 'currentToRight');
        $('#documents').attr('class', 'leftToCurrent');
        clear();
    }

    function getSelection() {
        var selection = jcrop.tellSelect();

        if (state.rotation === 90) {
            selection = {
                x: selection.y,
                y: $picture[0].width - selection.w - selection.x,
                w: selection.h,
                h: selection.w
            };
        }
        else if (state.rotation === 270) {
            selection = {
                x: $picture[0].height - selection.h  - selection.y,
                y: selection.x,
                w: selection.h,
                h: selection.w
            };
        }
        return {
            x: selection.x * pictureInfo.ratio,
            y: selection.y * pictureInfo.ratio,
            w: Math.min(selection.w * pictureInfo.ratio, pictureInfo.width),
            h: Math.min(selection.h * pictureInfo.ratio, pictureInfo.height)
        };
    }

    return {
        clear: clear,
        getSelection: getSelection,
        load: load,
        open: open,
        pictureInfo: pictureInfo,
        state: state
    };
}
