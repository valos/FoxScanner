/**
 * FoxScanner
 *
 * written by Valéry Febvre
 * vfebvre@aester-eggs.com
 *
 * Copyright 2015 Valéry Febvre
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var App = {
    name: 'FoxScanner',

    db: null,
    sdcard: null,
    picturesStorage: null,

    documents: null,
    cropper: null,
    enhancer: null,
    document: null,

    $currentSection: $('#documents'),

    // Initialize and start app
    start: function() {
        // about
        $('#about-open').on('click', function () {
            $('#about').attr('class', 'leftToCurrent');
            $('#documents').attr('class', 'currentToRight');
        });
        $('#about-close').on('click', function () {
            $('#about').attr('class', 'currentToLeft');
            $('#documents').attr('class', 'rightToCurrent');
        });

        navigator.mozL10n.once(function() {
            App.activityIndicator = FoxScannerActivityIndicator();
            App.documents = FoxScannerDocuments();
            App.cropper = FoxScannerCropper();
            App.enhancer = FoxScannerEnhancer();
            App.document = FoxScannerDocument();

            App.sdcard = navigator.getDeviceStorage("sdcard");
            App.picturesStorage = navigator.getDeviceStorage("pictures");

            var request = window.indexedDB.open('foxscanner-db', 1);
            request.onsuccess = function(e) {
                App.db = e.target.result;
                App.documents.load();
            };
            request.onupgradeneeded = function(e) {
                App.db = e.target.result;
                var store = App.db.createObjectStore('documents', {keyPath: 'id', autoIncrement: true});
                store.createIndex('updated', 'updated', {unique: false});
            };
        });
    }
};

window.addEventListener('load', App.start, false);
