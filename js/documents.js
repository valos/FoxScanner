/**
 * FoxScanner
 *
 * written by Valéry Febvre
 * vfebvre@aester-eggs.com
 *
 * Copyright 2015 Valéry Febvre
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function FoxScannerDocuments() {
    var $section = $('#documents');

    $("#picture-take").on('click', takePicture);

    function takePicture() {
        var activity = new MozActivity({
            name: "pick",
            data: {
                nocrop: true,
                type: ["image/png", "image/jpg", "image/jpeg"]
            }
        });

        activity.onsuccess = function() {
            var pictureUrl = window.URL.createObjectURL(this.result.blob);
            App.cropper.load(pictureUrl);
        };
    }

    function load() {
        var documentsInStorage = {};
        var cursor = App.sdcard.enumerate(App.name);

        // iterate through sdcard to list documents in storage
        cursor.onsuccess = function () {
            if (this.result) {
                var file = this.result;
                var path, id, filename;

                path = file.name;
                if (App.sdcard.storageName) {
                    // storage area is in SD card (not internal)
                    path = path.replace('/' + App.sdcard.storageName + '/', '');
                }

                path = path.split('/');
                id = path[1];
                filename = path[2];

                if (filename !== 'thumbnail._jpg') {
                    if (!documentsInStorage[id]) {
                        documentsInStorage[id] = {
                            id: parseInt(id, 10),
                            title: navigator.mozL10n.get('new-document') + ' ' + id,
                            filenames: [],
                            created: null,
                            updated: null
                        };
                    }
                    documentsInStorage[id].filenames.push(filename);
                    if (documentsInStorage[id].created === null || documentsInStorage[id].created > file.lastModifiedDate) {
                        documentsInStorage[id].created = file.lastModifiedDate;
                    }
                    if (documentsInStorage[id].updated === null || documentsInStorage[id].updated < file.lastModifiedDate) {
                        documentsInStorage[id].updated = file.lastModifiedDate;
                    }
                }
                this.continue();
            }
            else {
                // end of iteration through storage area
                var transaction = App.db.transaction(['documents'], 'readwrite');
                var store = transaction.objectStore('documents');

                // step through all values in object store
                store.openCursor().onsuccess = function(e) {
                    var cursor = e.target.result;
                    var counter;
                    var doc;

                    if (cursor) {
                        doc = cursor.value;
                        // removed document of documentsInStorage
                        delete documentsInStorage[doc.id];

                        cursor.continue();
                    }
                    else {
                        // reached end of object store
                        if (Object.keys(documentsInStorage).length === 0) {
                            render();
                        }
                        else {
                            // some documents in documentsInStorage are not in DB and must be restored
                            counter = 0;
                            $.each(documentsInStorage, function(i, doc) {
                                var request = store.add(doc);
                                request.onsuccess = function(e) {
                                    counter += 1;
                                    if (counter === Object.keys(documentsInStorage).length) {
                                        utils.status.show(navigator.mozL10n.get('db-restored'));
                                        render();
                                    }
                                };
                            });
                        }
                    }
                };
            }
        };
    }

    function render() {
        var transaction = App.db.transaction(['documents']);
        var store = transaction.objectStore('documents', 'readonly');
        var index = store.index('updated');
        var $list = $('#documents-list', $section);

        function renderItem(record, thumbnailFile) {
            var src = thumbnailFile ? window.URL.createObjectURL(thumbnailFile) : null;
            var updated = record.updated.toLocaleDateString() + ' ' + record.updated.toLocaleTimeString();
            var $li = $([
                '<li>',
                '<aside class="pack-end">',
                src ? '<img src="' + src + '">' : '',
                '</aside>',
                '<a class="document-open" data-id="' + record.id + '" href="#">',
                '<p>' + record.title + '</p>',
                '<p>' + updated + ' <span class="counter">' + record.filenames.length + '</span></p>',
                '</a>',
                '</li>'
            ].join(''));

            $li.find('a').on('click', function(event) {
                App.document.load($(this).data('id'));
            });

            $list.prepend($li);

            if (src) {
                window.URL.revokeObjectURL(src);
            }
        }

        $list.empty();
        index.openCursor().onsuccess = function(e) {
            var cursor = e.target.result;

            if (cursor) {
                var request = App.sdcard.get(App.document.getFilepath(cursor.value.id, 'thumbnail._jpg'));
                var value = cursor.value;
                request.onsuccess = function() {
                    renderItem(value, this.result);
                };

                request.onerror = function() {
                    renderItem(value);
                    utils.status.show("Unable to get thumbnail file");
                };

                cursor.continue();
            }
        };
    }

    return {
        load: load,
        render: render,
        takePicture: takePicture
    };
}
