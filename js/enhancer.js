/**
 * FoxScanner
 *
 * written by Valéry Febvre
 * vfebvre@aester-eggs.com
 *
 * Copyright 2015 Valéry Febvre
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

function FoxScannerEnhancer() {
    var $section = $('#enhancer');
    var $equalizerPanel = $('.enhancer-equalizer-panel');
    var glfx = null;
    var texture = null;
    var state = null;

    // toolbar top buttons
    $('#enhancer-grayscale').on('click', function() {
        setPictureGrayscale();
    });
    $('#enhancer-bright').on('click', function() {
        setPictureBright();
    });
    $('#enhancer-original').on('click', function() {
        setPictureOriginal();
    });

    // toolbar bottom buttons
    $('#enhancer-close').on('click', function() {
        close();
    });
    $('#enhancer-rotate-ccw').on('click', function() {
        rotatePicture('ccw');
    });
    $('#enhancer-rotate-cw').on('click', function() {
        rotatePicture('cw');
    });
    $('#enhancer-equalizer').on('click', function() {
        if ($equalizerPanel.hasClass('bottomToCurrent')) {
            closeEqualizer();
        }
        else {
            openEqualizer();
        }
    });
    $('#enhancer-validate').on('click', function(e) {
        var image = new Image();
        image.onload = function() {
            var canvas = document.createElement("canvas");
            var ctx = canvas.getContext('2d');
            var selection = App.cropper.getSelection();
            var width, height;

            if (state.rotation === 0 || state.rotation === 180) {
                canvas.width = selection.w;
                canvas.height = canvas.width * (selection.h / selection.w);
            }
            else {
                canvas.width = selection.h;
                canvas.height = canvas.width * (selection.w / selection.h);
            }

            if (state.rotation > 0) {
                if (state.rotation === 90) {
                    ctx.translate(canvas.width, 0);
                }
                else if (state.rotation === 180) {
                    ctx.translate(canvas.width, canvas.height);
                }
                else if (state.rotation === 270) {
                    ctx.translate(0, canvas.height);
                }
                ctx.rotate(state.rotation * Math.PI / 180);
            }

            if (state.rotation === 0 || state.rotation === 180) {
                width = canvas.width;
                height = canvas.height;
            }
            else {
                width = canvas.height;
                height = canvas.width;
            }
            ctx.drawImage(image, selection.x, selection.y, selection.w, selection.h, 0, 0, width, height);

            glfx = fx.canvas();
            texture = glfx.texture(canvas);
            render();

            App.document.createOrAddPicture(glfx.toDataURL('image/jpeg'), canvas.width, canvas.height);
        };

        App.activityIndicator.start();
        window.setTimeout(function() {
            image.src = App.cropper.pictureInfo.url;
        }, 50);
    });

    // equalizer panel sliders
    $('#enhancer-contrast', $equalizerPanel).on('change', function() {
        adjustPictureContrast($(this).val());
    });
    $('#enhancer-contrast-minus', $equalizerPanel).on('click', function() {
        $('#enhancer-contrast').val(parseInt($('#enhancer-contrast').val(), 10) - 1).trigger('change');
    });
    $('#enhancer-contrast-plus', $equalizerPanel).on('click', function() {
        $('#enhancer-contrast').val(parseInt($('#enhancer-contrast').val(), 10) + 1).trigger('change');
    });
    $('#enhancer-brightness', $equalizerPanel).on('change', function() {
        adjustPictureBrightness($(this).val());
    });
    $('#enhancer-brightness-minus', $equalizerPanel).on('click', function() {
        $('#enhancer-brightness').val(parseInt($('#enhancer-brightness').val(), 10) - 1).trigger('change');
    });
    $('#enhancer-brightness-plus', $equalizerPanel).on('click', function() {
        $('#enhancer-brightness').val(parseInt($('#enhancer-brightness').val(), 10) + 1).trigger('change');
    });
    $('#enhancer-threshold', $equalizerPanel).on('change', function() {
        adjustPictureThreshold($(this).val());
    });
    $('#enhancer-threshold-minus', $equalizerPanel).on('click', function() {
        $('#enhancer-threshold').val(parseInt($('#enhancer-threshold').val(), 10) - 1).trigger('change');
    });
    $('#enhancer-threshold-plus', $equalizerPanel).on('click', function() {
        $('#enhancer-threshold').val(parseInt($('#enhancer-threshold').val(), 10) + 1).trigger('change');
    });

    $('.picture-container', $section).on('click', function() {
        closeEqualizer();
    });

    function clear() {
        closeEqualizer();
        if (texture) {
            texture.destroy();
            $('#enhancer-picture').remove();
        }
    }

    function load(rotation) {
        var image;

        try {
            glfx = fx.canvas();
            glfx.id = 'enhancer-picture';
        }
        catch (e) {
            utils.status.show(e);
            return;
        }

        if (rotation === undefined) {
            state = {
                rotation: App.cropper.state.rotation || 0,
                brightness: 0,
                contrast: 0,
                threshold: 0,
                saturation: 0,
                exposure: 0,
                vibrance: 0
            };
            updateEqualizer();
        }
        else {
            state.rotation = rotation;
        }

        App.activityIndicator.start();
        image = new Image();
        image.onload = function() {
            var canvas = document.createElement("canvas");
            var canvasResized = document.createElement("canvas");
            var ctx = canvas.getContext('2d');
            var selection = App.cropper.getSelection();
            var scale;

            if (state.rotation === 0 || state.rotation === 180) {
                canvas.width = selection.w;
                canvas.height = canvas.width * (selection.h / selection.w);
            }
            else {
                canvas.width = selection.h;
                canvas.height = canvas.width * (selection.w / selection.h);
            }
            scale = $('.picture-container', $section).width() / canvas.width;
            canvasResized.width = canvas.width * scale;
            canvasResized.height = canvas.height * scale;

            if (state.rotation > 0) {
                if (state.rotation === 90) {
                    ctx.translate(canvas.width, 0);
                }
                else if (state.rotation === 180) {
                    ctx.translate(canvas.width, canvas.height);
                }
                else if (state.rotation === 270) {
                    ctx.translate(0, canvas.height);
                }
                ctx.rotate(state.rotation * Math.PI / 180);
            }

            if (state.rotation === 0 || state.rotation === 180) {
                ctx.drawImage(image, selection.x, selection.y, selection.w, selection.h, 0, 0, canvas.width, canvas.height);
            }
            else {
                ctx.drawImage(image, selection.x, selection.y, selection.w, selection.h, 0, 0, canvas.height, canvas.width);
            }

            // high quality image resize (Lanczos window 3px) with Pica library
            // canvas drawImage() function is using a linear-interpolation, nearest-neighbor resampling method
            // that doesn't work well when resize is too important (more than half the original size)
            window.pica.resizeCanvas(canvas, canvasResized, 3, function(err) {
                App.activityIndicator.stop();

                if (err) {
                    utils.status.show(err);
                }
                else {
                    // convert canvas to a texture
                    texture = glfx.texture(canvasResized);
                    render();

                    $(".picture-container", $section).append(glfx);

                    if (rotation === undefined) {
                        open();
                    }
                }
            });
        };
        image.src = App.cropper.pictureInfo.url;
    }

    function open() {
        App.$currentSection.attr('class', 'currentToLeft');
        $section.attr('class', 'rightToCurrent');
        App.$currentSection = $section;
    }

    function openEqualizer() {
        $equalizerPanel.removeClass('currentToBottom');
        $equalizerPanel.addClass('bottomToCurrent');
    }

    function close() {
        App.cropper.open('enhancer');
        clear();
    }

    function closeEqualizer() {
        $equalizerPanel.removeClass('bottomToCurrent');
        $equalizerPanel.addClass('currentToBottom');
    }

    function render() {
        glfx.draw(texture);

        if (state.saturation !== 0) {
            glfx.hueSaturation(0, state.saturation);
        }
        if (state.exposure !== 0) {
            glfx.exposure(state.exposure);
        }
        if (state.brightness !== 0 || state.contrast !== 0) {
            glfx.brightnessContrast(state.brightness, state.contrast);
        }
        if (state.threshold !== 0) {
            glfx.threshold(state.threshold);
        }

        updateEqualizer();

        glfx.update();
    }

    function updateEqualizer() {
        $equalizerPanel.find('h2').hide();
        $('#enhancer-contrast').val(state.contrast * 100);
        $('#enhancer-brightness').val(state.brightness * 100);
        $('#enhancer-threshold').val(state.threshold);
    }

    function adjustPictureBrightness(value) {
        state.brightness = parseInt(value, 10) / 100;
        render();
        $equalizerPanel.find('h2').text('Brightness: ' + value).show();
    }

    function adjustPictureContrast(value) {
        state.contrast = parseInt(value, 10) / 100;
        render();
        $equalizerPanel.find('h2').text('Contrast: ' + value).show();
    }

    function adjustPictureThreshold(value) {
        state.saturation = -1;
        state.threshold = parseInt(value, 10);
        render();
        $equalizerPanel.find('h2').text('Threshold: ' + value).show();
    }

    function rotatePicture(direction) {
        var rotation = state.rotation;
        if (direction === 'cw') {
            rotation += 90;
        }
        else if (direction === 'ccw') {
            rotation -= 90;
        }
        if (rotation < 0) {
            rotation += 360;
        }
        rotation %= 360;

        clear();
        load(rotation);
    }

    function setPictureBright() {
        state.brightness = 0.07;
        state.contrast = 0.16;
        state.threshold = 0;
        state.saturation = 0;
        state.exposure = 0.05;
        state.vibrance = 0.07;

        render();
    }

    function setPictureGrayscale() {
        state.brightness = 0;
        state.contrast = 0;
        state.threshold = 0;
        state.saturation = -1;
        state.exposure = 0;
        state.vibrance = 0;

        render();
    }

    function setPictureOriginal() {
        state.brightness = 0;
        state.contrast = 0;
        state.threshold = 0;
        state.saturation = 0;
        state.exposure = 0;
        state.vibrance = 0;

        render();
    }

    return {
        clear: clear,
        load: load
    };
}
